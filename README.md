# 无名微博客PHP版
# 这里不再更新，请移步：https://github.com/allures/
![输入图片说明](https://images.gitee.com/uploads/images/2019/0508/091141_e1378c08_2191229.jpeg "截图_2.jpg")

#### 介绍

无名轻博客v1.1正式版发布
运行环境：PHP>=5.4 pdo_sqlite

#### 更新如下：


- 1.编辑器优化+汉化
- 2.友好提示，大部分不再弹框
- 3.删除文章图片清理
- 4.重新做了一套模板,微调了模板结构
- 5.小程序接入
- 6.修复只传图导致右侧文章标题为空
- 7.其它细节调整
- 8.已知BUG修复


#### 使用前请修改密码


- 1、默认密码：admin
- 2、配置文件：app/class/config.php
- 3、wx.php为小程序接口文件，up.php为预览版升级程序，两个文件无用可以删除。


#### 安装教程

1. 下载 [点击下载](https://gitee.com/daimaguo/wmphp/blob/master/blogv1.1.zip)
2. 解压
3. 使用

#### 官网及演示

官方网站：https://www.4jax.net/log
演示网站：http://demo.semlog.cn/

欢迎体验使用提交BUG及建议。